image: node:16.6-alpine3.14

stages:
  - verify
  - prepare
  - build
  - release

variables:
  ENV_NAME: .env

verify:
  stage: verify
  environment:
    name: production
  only:
    - tags
  variables:
    VERIFICATION_BRANCH: ci-verify
  before_script:
    - apk add git
    - git checkout -b $VERIFICATION_BRANCH
  script:
    - echo "Checking if tag source is from default branch '$CI_DEFAULT_BRANCH'..."
    - if [ ! -z "$(sh ci/scripts/log-difference-with-remote.sh $CI_REPOSITORY_URL $CI_DEFAULT_BRANCH $VERIFICATION_BRANCH)" ]; then echo "Tag source is not from $CI_DEFAULT_BRANCH" && exit 1; fi;
    - echo "Checking commit tag semantic versioning..."
    - npm version --no-git-tag-version $CI_COMMIT_TAG

prepare:
  stage: prepare
  environment:
    name: production
  only:
    - tags
  script:
    - cp $ENV $ENV_NAME
  artifacts:
    name: $CI_JOB_NAME-ARTIFACTS
    expire_in: 10 mins
    paths:
      - $ENV_NAME

build:
  stage: build
  environment:
    name: production
  only:
    - tags
  dependencies:
    - prepare
  before_script:
    - (cd /bin && mv gzip gzip-busybox.bak && apk add --update gzip)
    - yarn --prefer-offline
  script:
    - yarn generate
    # Compress production package except images (.jpg and .png)
    - for file in `find public -type f ! -name '*\.jpg' ! -name '*.png'`; do gzip -k9v "$file" ; done
    - cat $GSC_VERIFICATION_MAP | while read file; do reference=${file%% *}; publicFilename=public/${file##* }; cp $(eval echo \$$reference) $publicFilename; done;
  artifacts:
    name: $CI_JOB_NAME-ARTIFACTS
    expire_in: 10 mins
    paths:
      - public

pages:
  stage: release
  environment:
    name: production
    url: $CI_PAGES_URL
  only:
    - tags
  dependencies:
    - build
  script:
    - echo "Releasing portfolio to $CI_PAGES_URL"
  artifacts:
    paths:
      - public

release:
  stage: release
  environment:
    name: production
  only:
    - tags
  before_script:
    - apk add git
    - DEPLOYMENT_URL=$(sh ci/scripts/generate-deployment-url.sh $CI_REPOSITORY_URL $GITLAB_ACCESS_TOKEN)
    - git config --global user.name "$CI_GIT_USERNAME"
    - git config --global user.email "$CI_GIT_EMAIL"
    - git checkout -b ci-bump-version
  script:
    - echo "Bumping package version to $CI_COMMIT_TAG"
    - npm version --no-git-tag-version $CI_COMMIT_TAG
    - echo "Committing published version $CI_COMMIT_TAG to master"
    - git add package.json
    - git commit -m "Release $CI_COMMIT_TAG"
    - git push $DEPLOYMENT_URL HEAD:$CI_DEFAULT_BRANCH
