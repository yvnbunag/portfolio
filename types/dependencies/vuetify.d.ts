declare module 'vuetify/lib/components/VGrid' {
  import type * as components from 'vuetify/lib/components'

  const VContainer: typeof components.VContainer
  const VRow: typeof components.VRow
  const VCol: typeof components.VCol
}

declare namespace VuetifyProps {
  interface VRow {
    justify: 'start' | 'center' | 'end' | 'space-between' | 'space-around',
  }
}
