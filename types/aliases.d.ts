/**
 * Alias for a Vue component prop
 */
type Prop<Type> = ()=> Type
