export type Icon = string
export type IconMap = Record<string, Icon>
