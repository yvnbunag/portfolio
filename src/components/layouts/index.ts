import AppBar from '~/components/layouts/app-bar.vue'
import AppFooter from '~/components/layouts/app-footer.vue'

export {
  AppBar,
  AppFooter,
}
