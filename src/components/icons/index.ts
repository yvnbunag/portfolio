import adobeAfterEffects from '~/components/icons/adobe-after-effects.vue'
import adobeAudition from '~/components/icons/adobe-audition.vue'
import adobeIllustrator from '~/components/icons/adobe-illustrator.vue'
import adobePhotoshop from '~/components/icons/adobe-photoshop.vue'
import adobePremierPro from '~/components/icons/adobe-premier-pro.vue'
import autodeskMaya from '~/components/icons/autodesk-maya.vue'
import awsChalice from '~/components/icons/aws-chalice.vue'
import backEnd from '~/components/icons/back-end.vue'
import bootstrapVue from '~/components/icons/bootstrap-vue.vue'
import circleCI from '~/components/icons/circle-ci.vue'
import codewars from '~/components/icons/codewars.vue'
import cypress from '~/components/icons/cypress.vue'
import devOps from '~/components/icons/dev-ops.vue'
import express from '~/components/icons/express.vue'
import fastify from '~/components/icons/fastify.vue'
import frontEnd from '~/components/icons/front-end.vue'
import jest from '~/components/icons/jest.vue'
import leetcode from '~/components/icons/leetcode.vue'
import lumen from '~/components/icons/lumen.vue'
import mysql from '~/components/icons/mysql.vue'
import nextjs from '~/components/icons/nextjs.vue'
import nginx from '~/components/icons/nginx.vue'
import photopea from '~/components/icons/photopea.vue'
import pm2 from '~/components/icons/pm2.vue'
import stylelint from '~/components/icons/stylelint.vue'
import swagger from '~/components/icons/swagger.vue'
import xampp from '~/components/icons/xampp.vue'
import ianbunagDock from '~/components/icons/ianbunag-dock.vue'
import vitest from '~/components/icons/vitest.vue'
import postgresql from '~/components/icons/postgresql.vue'
import reactNative from '~/components/icons/react-native.vue'
import bluesky from '~/components/icons/bluesky.vue'

export {
  adobeAfterEffects,
  adobeAudition,
  adobeIllustrator,
  adobePhotoshop,
  adobePremierPro,
  autodeskMaya,
  awsChalice,
  backEnd,
  bootstrapVue,
  circleCI,
  codewars,
  cypress,
  devOps,
  express,
  fastify,
  frontEnd,
  jest,
  leetcode,
  lumen,
  mysql,
  nextjs,
  nginx,
  photopea,
  pm2,
  stylelint,
  swagger,
  xampp,
  ianbunagDock,
  vitest,
  postgresql,
  reactNative,
  bluesky,
}
