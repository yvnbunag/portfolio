import { createUnorderedList, requireProgressiveImage } from '~/lib/config'
import { LinkName, mapReferencedProjects } from '~/lib/config/profile/projects'
import { createPeriod } from '~/lib/config/profile'

export const applicationSoftwares = mapReferencedProjects([
  {
    name: '@ianbunag/scaffold',
    key: 'ianbunag-scaffold',
    period: createPeriod(2023, 'July'),
    tags: [
      'CLI',
      'TypeScript',
      'npm',
      'software',
      'application',
      'scaffold',
      'boilerplate',
      'generator',
    ],
    description: createUnorderedList([
      'Designed a CLI tool, "Scaffold", to streamline front-end and back-end project launches with JavaScript, offering functionalities for initialization, directory structure creation, dependency installation, and configuration setup.',
      'Simplified foundational setup, enhancing coding challenge efficiency with automation tools to ensure standardized structure and offer clear documentation and tutorials.',
    ]),
    images: [
      './assets/images/projects/scaffold/initial.png',
      './assets/images/projects/scaffold/scaffolded.png',
      './assets/images/projects/scaffold/setup.png',
      './assets/images/projects/scaffold/test.png',
      './assets/images/projects/scaffold/development.png',
      './assets/images/projects/scaffold/production.png',
    ].map(requireProgressiveImage),
    technologies: ['node-js', 'npm', 'gitlab'],
    featured: true,
    secondaryLink: {
      text: LinkName.VIEW_SOURCE,
      link: process.env.SCAFFOLD_REPOSITORY,
      linkIcon: 'mdiGitlab',
    },
    primaryLink: {
      text: LinkName.TRY_ME,
      link: process.env.SCAFFOLD_NPM_URL,
      linkIcon: 'mdiConsole',
    },
  },
  {
    name: '@ianbunag/dock',
    key: 'ianbunag-dock',
    period: createPeriod(2021, 'August'),
    tags: [
      'CLI',
      'BDD',
      'TDD',
      'TypeScript',
      'docker',
      'docker-compose',
      'dock',
      'npm',
      'software',
      'application',
      'completion',
    ],
    description: createUnorderedList([
      'Created a CLI tool to optimize containerized development environment management using TypeScript and Docker.',
      'Enabled handling of multi-service containers using lightweight container orchestration tools, thereby optimizing container images, particularly for individuals facing computational constraints on their work/personal computers.',
    ]),
    images: [
      './assets/images/projects/dock/root-help.png',
      './assets/images/projects/dock/projects.png',
      './assets/images/projects/dock/build.png',
      './assets/images/projects/dock/root-command.png',
      './assets/images/projects/dock/open.png',
      './assets/images/projects/dock/run.png',
      './assets/images/projects/dock/mode.png',
    ].map(requireProgressiveImage),
    technologies: [
      'typescript',
      'jest',
      'webpack',
      'node-js',
      'docker',
      'npm',
      'gitlab',
    ],
    featured: true,
    secondaryLink: {
      text: LinkName.VIEW_SOURCE,
      link: process.env.DOCK_REPOSITORY,
      linkIcon: 'mdiGitlab',
    },
    primaryLink: {
      text: LinkName.TRY_ME,
      link: process.env.DOCK_NPM_URL,
      linkIcon: 'mdiConsole',
    },
  },
  {
    name: 'Simple Calculator',
    key: 'simple-calculator',
    period: createPeriod(2015, 'October'),
    tags: ['software', 'application', 'university', 'programming'],
    description:
      'Developed a user-friendly calculator application capable of performing basic arithmetic operations. Implemented features such as type or click input, clear functionality, value negation, and system message prompts to enhance usability and functionality.',
    images: [
      './assets/images/projects/simple-calculator/initial.png',
      './assets/images/projects/simple-calculator/input.png',
      './assets/images/projects/simple-calculator/solve.png',
      './assets/images/projects/simple-calculator/delete.png',
      './assets/images/projects/simple-calculator/clear.png',
    ].map(requireProgressiveImage),
    technologies: ['java'],
    secondaryLink: {
      text: LinkName.VIEW_SOURCE,
      link: process.env.SIMPLE_CALCULATOR_REPOSITORY,
      linkIcon: 'mdiGitlab',
    },
  },
  {
    name: 'Simple Circuit Calculator',
    key: 'simple-circuit-calculator',
    period: createPeriod(2015, 'October'),
    tags: ['software', 'application', 'university', 'physics'],
    description:
      'Engineered a circuit calculator application facilitating calculations for total resistance in both series and parallel circuits, adhering to Ohm\'s Law Physics equation. Integrated features such as auto-fill for missing and constant variables, streamlining the calculation process and ensuring accuracy in results.',
    images: [
      './assets/images/projects/simple-circuit-calculator/initial.png',
      './assets/images/projects/simple-circuit-calculator/series-constant-auto-solve.png',
      './assets/images/projects/simple-circuit-calculator/series-missing-variable-auto-solve.png',
      './assets/images/projects/simple-circuit-calculator/series-remaining-variable-auto-solve.png',
      './assets/images/projects/simple-circuit-calculator/series-total.png',
      './assets/images/projects/simple-circuit-calculator/parallel-missing-variable-auto-solve.png',
      './assets/images/projects/simple-circuit-calculator/parallel-remaining-variable-auto-solve.png',
      './assets/images/projects/simple-circuit-calculator/parallel-total.png',
    ].map(requireProgressiveImage),
    technologies: ['java'],
    secondaryLink: {
      text: LinkName.VIEW_SOURCE,
      link: process.env.SIMPLE_CIRCUIT_CALCULATOR_REPOSITORY,
      linkIcon: 'mdiGitlab',
    },
  },
  {
    name: `Ohm's Law Calculator`,
    key: 'ohms-law-calculator',
    period: createPeriod(2015, 'October'),
    tags: ['software', 'application', 'university', 'physics'],
    description: 'Developed a versatile Ohm\'s Law calculator application enabling computation of Voltage (V), Ampere (I), or Resistance (Ω) based on the Ohm\'s Law Physics equation. Designed for ease of use, the application empowers users to input known variables and calculate the desired parameter with precision and efficiency.',
    images: [
      './assets/images/projects/ohms-law-calculator/initial.png',
      './assets/images/projects/ohms-law-calculator/calculate-voltage.png',
      './assets/images/projects/ohms-law-calculator/calculate-ampere.png',
      './assets/images/projects/ohms-law-calculator/calculate-resistance.png',
    ].map(requireProgressiveImage),
    technologies: ['java'],
    secondaryLink: {
      text: LinkName.VIEW_SOURCE,
      link: process.env.OHMS_LAW_CALCULATOR_REPOSITORY,
      linkIcon: 'mdiGitlab',
    },
  },
])
